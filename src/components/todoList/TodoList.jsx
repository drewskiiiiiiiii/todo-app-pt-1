import React from 'react';
import TodoItem from '../todoItem/TodoItem';
import { v4 } from 'uuid';


const TodoList = ({ todos, setTodos }) => {
  return (
    <section className = "main" >
      <ul className = "todo-list" > {
          todos.map((todo) => (
            <TodoItem
              key = {v4()}
              title = { todo.title }
              completed = { todo.completed }
              setTodos = {setTodos}
              todos = {todos}
            />
          ))
      } </ul>
    </section>
  );
}

export default TodoList