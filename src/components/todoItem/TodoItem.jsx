import React from 'react';

const TodoItem = ({ title, completed, setTodos, todos }) => {
    const handleToggle = () => {
        let copyArray = todos.slice();
        copyArray = copyArray.map((todo) => todo.title === title ? {
            userId: 1,
            id: 1,
            title: todo.title,
            completed: !completed,
          } : todo)
        setTodos(copyArray)
    }

    const handleDelete = () => {
        let newArray = todos.filter((todo) => todo.title !== title);
        setTodos(newArray)
    }

    return ( 
    <li className = { completed ? "completed" : "" }>
        <div className = "view" >
            <input className = "toggle"
                type = "checkbox"
                checked = { completed }
                onChange = { handleToggle }
            />
            <label> { title } </label>
            <button className = "destroy"
                onClick={handleDelete}
            />
        </div>
    </li>
    );
};

export default TodoItem