import React, { useState } from "react";
import todosList from "./todos.json";
import TodoList from "./components/todoList/TodoList";

const App = () => {
  const [todos, setTodos] = useState(todosList);
  const [typedText, setTypedText] = useState('');
  const todosLeft = todos.filter((todo) => !todo.completed).length;
  const clearCompleted = () => {
    setTodos(todos.filter((todo) => !todo.completed))
  }
  const newTodoItem = {
      userId: 1,
      id: 1,
      title: typedText,
      completed: false,
    };

  const handleSubmit = (e) => {
    e.preventDefault();
    const todoInput = document.getElementById('todoInput');
    const copyArray = todos.slice();
    copyArray.push(newTodoItem)
    setTodos(copyArray)
    todoInput.value=''
  };

  const handleChange = (e) => {
    setTypedText(e.target.value)
  }

    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <form onSubmit={handleSubmit}>
          <input
            type="text/submit"
            className="new-todo"
            id='todoInput'
            placeholder="What needs to be done?"
            autoFocus
            onChange={handleChange}
          />
          </form>
        </header>
        <TodoList todos={todos} setTodos={setTodos} />
        <footer className="footer">
          <span className="todo-count">
            <strong>{todosLeft}</strong> item(s) left
          </span>
          <button className="clear-completed"
            onClick={clearCompleted}
          >
          Clear completed</button>
        </footer>
      </section>
    );
  }


export default App;
